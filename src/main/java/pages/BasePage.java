package pages;

import io.appium.java_client.AppiumDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.ReporterConfig;
import utils.support.SharedKey;
import utils.ui.MobileAction;

import java.util.HashMap;

import static utils.FrameworkUtils.*;

public class BasePage extends MobileAction {
    public static AppiumDriver driver;
    protected static HashMap<SharedKey, Object> commonObjectMap = new HashMap();
    protected static HashMap<String, Object> globalVariables = new HashMap<String, Object>();
    private static final Logger LOGGER = LoggerFactory.getLogger(BasePage.class);


    public BasePage(AppiumDriver driver) {
        this.driver = driver;
        System.setProperty("org.uncommons.reportng.escape-output", "false");
//        System.setProperty("org.uncommons.reportng.coverage-report", "../..target");
    }

    public static <T extends BasePage> T getSharedPage(SharedKey key, Class<T> type) {
        return type.cast(commonObjectMap.get(key));
    }

    public static void setSharedPage(SharedKey keys, BasePage value) {
        commonObjectMap.put(keys, value);
    }

    public void setLabelValue(String key, Object value) {
        LOGGER.info("Global variable is set with key as : " + key + " and value as: " + value);
        globalVariables.put(key, value);
    }

    public void logTestFailed(String file, String error){
        LOGGER.error("Verification failed : " + error);
        takeScreenshot(driver, file);
        Reporter.log("<td>Verification Failed: " + error + "<br></br><br></br><img src=" + "\"screenshots/" + file + ".png\" height=\"400\" width=\"250\""  + "</img>");
    }

    public void logPageLoadingError(String file, String error){
//        LOGGER.error("Page : " +  this.getClass().getName() + " not loaded properly due to the " + error);
        takeScreenshot(driver, file);
        Reporter.log("<td>Page Loading error: " + this.getClass().getName() + " not loaded properly due to the " + error + "<br></br><br></br><img src=" + "\"screenshots/" + file + ".png\" height=\"400\" width=\"250\""  + "</img>");
    }
}
