package Exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Banuka Liyanage created on 1/3/2018
 */
public class FrameworkException extends Exception {
    private static final Logger LOGGER = LoggerFactory.getLogger(FrameworkException.class);

    public FrameworkException(String message) {
        super(message);
        LOGGER.error(message);
    }

    public FrameworkException(String message, Exception e) {
        super(message, e);
        LOGGER.error(message, e);
    }
}
