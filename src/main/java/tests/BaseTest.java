package tests;

import Exceptions.FrameworkException;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import net.bytebuddy.implementation.bytecode.Throw;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import pages.BasePage;
import utils.driver.DriverConnection;
import utils.support.BasicSharedObjectKeys;
import utils.support.TestStrategy;

import java.io.IOException;

import static pages.BasePage.setSharedPage;

/**
 * @author Banuka Liyanage created on 12/28/2017
 */
public class BaseTest {
    public static AppiumDriver driver;
    private static final Logger LOGGER = LoggerFactory.getLogger(BaseTest.class);
    public static TestStrategy testStrategy;

    public BaseTest(TestStrategy ts){
        testStrategy = ts;
    }

    @BeforeSuite(alwaysRun = true)
    public void beforeMethod() throws FrameworkException{
        PropertyConfigurator.configure(getClass().getClassLoader().getResource("log4j.properties").getPath());
        driver = DriverConnection.getDriverConnection(testStrategy);
        if(driver == null){
            throw new FrameworkException("Driver connection failed with errors");
        }
        BasePage basePage = new BasePage(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), basePage);
        setSharedPage(BasicSharedObjectKeys.BASEPAGE, basePage);
    }


    @AfterSuite(alwaysRun = true)
    public void tearDown() throws SessionNotCreatedException, IOException, InterruptedException {
//        testSuiteExecutionCompleted = true;
        DriverConnection.closeDriver();
    }
}


