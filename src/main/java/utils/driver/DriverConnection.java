package utils.driver;

/**
 * @author Banuka Liyanage created on 12/27/2017
 */

import Exceptions.FrameworkException;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.aspectj.apache.bcel.classfile.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.support.TestStrategy;

public class DriverConnection {
    private static final Logger LOGGER = LoggerFactory.getLogger(DriverConnection.class);
    private static AppiumDriver driver;

    public DriverConnection() {
    }

    /**
     * 
     * @param testStrategy
     * @return
     */
    public static AppiumDriver getDriverConnection(TestStrategy testStrategy) {
        switch (testStrategy) {
            case ANDROIDNATIVEREALDEVICE:
                try {
                    driver = DriverBuilder.prepareAppiumForAndroidNativeRealDevice(driver);
                } catch (Throwable t) {
                    LOGGER.error("Driver initialising got errors.");
                    t.printStackTrace();
                }
                break;
            case IOSNATIVEREALDEVICE:
                try {
                    driver = DriverBuilder.prepareAppiumForIOSNativeRealDevice(driver);
                } catch (Throwable t) {
                    LOGGER.error("Driver initialising got errors.");
                    t.printStackTrace();
                }
                break;
            case ANDROIDWEBREALDEVICE:
                try {
                    driver = DriverBuilder.prepareAppiumForAndroidWEBRealDevice(driver);
                } catch (Throwable t) {
                    LOGGER.error("Driver initialising got errors.");
                    t.printStackTrace();
                }
                break;
            case ANDROIDWEBAVD:
                try {
                    driver = DriverBuilder.prepareAppiumForAndroidWEBAVD(driver);
                } catch (Throwable t) {
                    LOGGER.error("Driver initialising got errors.");
                    t.printStackTrace();
                }
                break;
        }
        return driver;
    }


    public static void closeDriver() {
        driver.closeApp();
    }


    public static AppiumDriver getDriver() {
        return driver;
    }
}