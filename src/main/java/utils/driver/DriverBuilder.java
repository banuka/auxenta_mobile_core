package utils.driver;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static utils.support.Properties.*;
import static utils.support.Properties.MOBILE_APP_ACTIVITY;
import static utils.support.Properties.MOBILE_APP_PACKAGE;

/**
 * @author Banuka Liyanage created on 12/27/2017
 */
public class DriverBuilder {
    private static final Logger LOGGER = LoggerFactory.getLogger(DriverBuilder.class);


    /**
     * Set Appium for Android native app in real device.
     * @param driver
     * @return AndroidDriver
     */
    public static AppiumDriver prepareAppiumForAndroidNativeRealDevice(AppiumDriver driver) {
        DesiredCapabilities capabilities = DesiredCapabilities.android();

        // mandatory capabilities
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, MOBILE_DEVICE);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MOBILE_PLATFORM_NAME);
        capabilities.setCapability(MobileCapabilityType.VERSION, MOBILE_PLATFORM_VERSION);

        capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, Integer.valueOf("150"));

        // other caps
        capabilities.setCapability(MobileCapabilityType.UDID, MOBILE_UDID);
        capabilities.setCapability("noReset", true);
        capabilities.setCapability("appPackage", MOBILE_APP_PACKAGE);
        capabilities.setCapability("appActivity", MOBILE_APP_ACTIVITY);

        URL url = null;
        try {
            url = new URL(System.getProperty("appium.server.url", "http://0.0.0.0:4723/wd/hub"));
        } catch (MalformedURLException e) {
            LOGGER.error("Connecting Appium server got failed.");
            e.printStackTrace();
        }

        driver = new AndroidDriver(url, capabilities);
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        return driver;
    }


    /**
     * Set Appium for IOS native app in real device.
     * @param driver
     * @return IOSDriver
     */
    public static AppiumDriver prepareAppiumForIOSNativeRealDevice(AppiumDriver driver) {
        DesiredCapabilities capabilities = DesiredCapabilities.android();
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.IOS);
        capabilities.setCapability(MobileCapabilityType.UDID, MOBILE_UDID);
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, MOBILE_DEVICE);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, MOBILE_PLATFORM_VERSION);

        URL url = null;
        try {
            url = new URL(System.getProperty("appium.server.url", "http://0.0.0.0:4723/wd/hub"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        driver = new AndroidDriver(url, capabilities);
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        return driver;

    }


    /**
     * Set Appium for Android mobile web in real device.
     * @param driver
     * @return AndroidDriver
     */
    public static AppiumDriver prepareAppiumForAndroidWEBRealDevice(AppiumDriver driver) {
        DesiredCapabilities capabilities = DesiredCapabilities.android();

        // mandatory capabilities
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, MOBILE_DEVICE);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MOBILE_PLATFORM_NAME);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, MOBILE_PLATFORM_VERSION );

        capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, Integer.valueOf("1500"));

        // other caps
        capabilities.setCapability(MobileCapabilityType.UDID, MOBILE_UDID);
        capabilities.setCapability("browserName", MOBILE_BROWSER_NAME);

        URL url = null;
        try {
            url = new URL(System.getProperty("appium.server.url", "http://0.0.0.0:4723/wd/hub"));
        } catch (MalformedURLException e) {
            LOGGER.error("Connecting Appium server got failed.");
            e.printStackTrace();
        }

        driver = new AndroidDriver(url, capabilities);
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        return driver;
    }


    /**
     * Set Appium for Android mobile web in emulator.
     * @param driver
     * @return AndroidDriver
     */
    public static AppiumDriver prepareAppiumForAndroidWEBAVD(AppiumDriver driver) {
        DesiredCapabilities capabilities = DesiredCapabilities.android();

        // mandatory capabilities
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, MOBILE_DEVICE);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MOBILE_PLATFORM_NAME);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, MOBILE_PLATFORM_VERSION );

        capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, Integer.valueOf("150"));

        // other caps
        capabilities.setCapability("avd", MOBILE_AVD);
        capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, MOBILE_BROWSER_NAME);

        URL url = null;
        try {
            url = new URL(System.getProperty("appium.server.url", "http://0.0.0.0:4723/wd/hub"));
        } catch (MalformedURLException e) {
            LOGGER.error("Connecting Appium server got failed.");
            e.printStackTrace();
        }

        driver = new AndroidDriver(url, capabilities);
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        return driver;
    }

}
