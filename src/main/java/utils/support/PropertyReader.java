package utils.support;

import Exceptions.FrameworkException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static tests.BaseTest.testStrategy;

public class PropertyReader {
    private static final Logger LOGGER = LoggerFactory.getLogger(PropertyReader.class);
    private static Properties properties;
    private static String PROPERTY_FILE_PATH;

    static {
        try {
            PROPERTY_FILE_PATH = PropertyReader.class.getClassLoader().getResource(testStrategy.getConfigPath()).getPath();
            properties = getProperties();
            LOGGER.info("App Properties got initialised.");
        } catch (Exception e) {
            LOGGER.error("Property file : " + testStrategy.getConfigPath() + " is not found in resources/mobile.");
            e.printStackTrace();
//            throw new RuntimeException("Error when loading the configuration file " + testStrategy.getConfigPath());
        }
    }


    /**
     *
     * @param propertyName
     * @return propertyValue
     */
    public static String getValue(String propertyName) {
        return getProperties().getProperty(propertyName);
    }

    /**
     *
     * @param propertyName
     * @param defaultValue
     * @return propertyValue
     */
    public static String getValue(String propertyName, String defaultValue) {
        return properties.getProperty(propertyName, defaultValue);
    }

    private static Properties getProperties() {
        InputStream inputStream = null;
        if (properties == null) {
            properties = new Properties();
            try {
                File file = new File(PROPERTY_FILE_PATH);
                inputStream = new FileInputStream(file);
                properties.load(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException("Error when loading the configuration file '" + PROPERTY_FILE_PATH + "'");
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                        throw new RuntimeException("Error when closing the configuration file '" + PROPERTY_FILE_PATH + "'");
                    }
                }
            }
        }
        return properties;
    }

}
