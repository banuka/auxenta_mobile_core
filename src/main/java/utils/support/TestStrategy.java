package utils.support;

/**
 * @author Banuka Liyanage created on 12/29/2017
 */
public enum TestStrategy {
    ANDROIDNATIVEREALDEVICE("mobile/android_native_device.properties"),
    IOSNATIVEREALDEVICE("mobile/ios_native_device.properties"),
    ANDROIDNATIVEAVD("mobile/android_native_avd.properties"),
    IOSNATIVESIMULATOR("mobile/ios_native_simulator.properties"),

    ANDROIDWEBREALDEVICE("mobile/android_web_device.properties"),
    IOSWEBREALDEVICE("mobile/ios_web_device.properties"),
    ANDROIDWEBAVD("mobile/android_web_avd.properties"),
    IOSWEBSIMULATOR("mobile/ios_web_simulator.properties"),

    ANDROIDHYBRIDREALDEVICE("mobile/android_hybrid_device.properties"),
    IOSHYBRIDREALDEVICE("mobile/ios_hybrid_device.properties"),
    ANDROIDHYBRIDAVD("mobile/android_hybrid_avd.properties"),
    IOSHYBRIDSIMULATOR("mobile/ios_hybrid_simulator.properties"),;

    private final String configPath;

    TestStrategy(String path){
        this.configPath = path;
    }

    public String getConfigPath() {
        return configPath;
    }
}
