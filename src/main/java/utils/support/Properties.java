package utils.support;

import static utils.support.PropertyReader.*;

public final class Properties {

    public static final String MOBILE_DEVICE = getValue("mobileDevice", "Banuka S4");
    public static final String MOBILE_PLATFORM_NAME = getValue("mobilePlatform", "ios");
    public static final String MOBILE_PLATFORM_VERSION = getValue("mobilePlatformVersion", "5.0.1");
    public static final String MOBILE_UDID = getValue("mobileUdid", "Android");
    public static final String MOBILE_AVD = getValue("mobileAVD", "Nexus_5_API_22");

    public static final String MOBILE_APP_PACKAGE = getValue("mobileAppPackage", "net.omobio.dialogsc");
    public static final String MOBILE_APP_ACTIVITY = getValue("mobileAppActivity", "net.omobio.dialogsc.MainActivity");

    public static final String MOBILE_BROWSER_NAME = getValue("mobileBrowserName", "Browser");


    public static final int SELENIUM_ELEMENT_LOCATING_TIME_OUT_SECONDS =  Integer.parseInt(getValue("selenium.elementTimeOut", "30"));
}
