package utils;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.InvalidServerInstanceException;

/**
 * @author Banuka Liyanage created on 1/5/2018
 */
public class AppiumUtils {


    private static AppiumDriverLocalService service;

    public static void startAppiumServer() {
//        service = AppiumDriverLocalService.buildService(new AppiumServiceBuilder()
//                .usingDriverExecutable(new File("/home/hiran/Software/nodejs/node-v5.9.1-linux-x64/bin/node"))
//                .withAppiumJS(new File("/home/hiran/Software/nodejs/node-v5.9.1-linux-x64/lib/node_modules/appium/bin/appium.js"))
//                .withIPAddress("127.0.0.1")
//                .usingPort(4723)
//                .withLogFile(new File("target/appium.log")));
        try {
            service = AppiumDriverLocalService.buildDefaultService();
            service.start();
        } catch (InvalidServerInstanceException e) {
            e.printStackTrace();
        }

    }

    public static void stopAppiumServer() {
        if (service != null && service.isRunning())
            service.stop();
    }

    public static void main(String[] args) {
        startAppiumServer();
    }
}
