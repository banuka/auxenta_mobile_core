package utils;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.BasePage;

import java.io.File;
import java.io.FileOutputStream;

/**
 * @author Banuka Liyanage created on 1/12/2018
 */
public class FrameworkUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(FrameworkUtils.class);
    public static void takeScreenshot(AppiumDriver driver, String fileName) {
        try {
            new File("../target/surefire-reports/screenshots").mkdirs();
            FileOutputStream out = new FileOutputStream("../target/surefire-reports/screenshots/" + fileName + ".png");
            out.write(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES));
            out.close();
            LOGGER.error("*****************************Successfully a screeshot was taken.************************************");
        } catch (Exception e) {
            LOGGER.error("Error while taking screenshot.");
            e.printStackTrace();
        }
    }
}
