package utils.ui;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;

/**
 * @author Banuka Liyanage created on 1/8/2018
 */
public class MobileAction extends SeleniumUtils {

    public void swipeLeft() {

    }

    public void swipeRight() {
    }

    public void swipeUp() {
        int height = driver.manage().window().getSize().getHeight();
        int width = driver.manage().window().getSize().getWidth();

        int starty = (int) (height * (0.6));
        int endy = (int) (height * (0.5));
        int startx = width / 2;
        int endx = width / 2;

        //To move from Fav to all contacts, we need to swipe from right to left
        TouchAction tap = new TouchAction(driver);

        tap.longPress(startx, starty).moveTo(endx, endy).release().perform();
    }

    public void swipeUp(int offSet) {
        int height = driver.manage().window().getSize().getHeight();
        int width = driver.manage().window().getSize().getWidth();

        int starty = (int) (height * (0.6));
        int endy = starty - offSet;
        int startx = width / 2;
        int endx = width / 2;

        //To move from Fav to all contacts, we need to swipe from right to left
        TouchAction tap = new TouchAction(driver);

        tap.longPress(startx, starty).moveTo(endx, endy).release().perform();
    }

    public void swipeUp(MobileElement element) {
        int height = driver.manage().window().getSize().getHeight();
        int width = driver.manage().window().getSize().getWidth();

        int starty = (int) (height * (0.5));
        int endy = (int) (height * (0.4));
        int startx = width / 2;
        int endx = width / 2;

        //To move from Fav to all contacts, we need to swipe from right to left
        TouchAction tap = new TouchAction(driver);

        tap.longPress(element).moveTo(endx, endy).release().perform();
    }

    public void swipeDown() {
        int height = driver.manage().window().getSize().getHeight();
        int width = driver.manage().window().getSize().getWidth();

        int endy = (int) (height * (0.6));
        int starty = (int) (height * (0.5));
        int startx = width / 2;
        int endx = width / 2;

        //To move from Fav to all contacts, we need to swipe from right to left
        TouchAction tap = new TouchAction(driver);

        tap.longPress(startx, starty).moveTo(endx, endy).release().perform();
    }

    public void swipeDown(int offSet) {
        int height = driver.manage().window().getSize().getHeight();
        int width = driver.manage().window().getSize().getWidth();

        int starty = (int) (height * (0.6));
        int endy = starty + offSet;
        int startx = width / 2;
        int endx = width / 2;

        //To move from Fav to all contacts, we need to swipe from right to left
        TouchAction tap = new TouchAction(driver);

        tap.longPress(startx, starty).moveTo(endx, endy).release().perform();
    }

    public void swipeDown(MobileElement element) {
        int height = driver.manage().window().getSize().getHeight();
        int width = driver.manage().window().getSize().getWidth();

        int endy = (int) (height * (0.5));
        int starty = (int) (height * (0.4));
        int startx = width / 2;
        int endx = width / 2;

        //To move from Fav to all contacts, we need to swipe from right to left
        TouchAction tap = new TouchAction(driver);

        tap.longPress(element).moveTo(endx, endy).release().perform();
    }

}
