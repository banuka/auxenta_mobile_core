package utils.ui;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import utils.driver.DriverConnection;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static utils.support.Properties.SELENIUM_ELEMENT_LOCATING_TIME_OUT_SECONDS;

/**
 * @author Banuka Liyanage created on 12/28/2017
 */
public class SeleniumUtils {
    protected static AppiumDriver driver;
    static {
        driver = DriverConnection.getDriver();
    }


    protected WebElement waitElementToBeClickable(By locator) throws WebDriverException {
        Wait wait = new FluentWait(driver)
                .withTimeout(SELENIUM_ELEMENT_LOCATING_TIME_OUT_SECONDS, SECONDS)
                .pollingEvery(100, MILLISECONDS)
                .ignoring(NoSuchElementException.class)
                .ignoring(StaleElementReferenceException.class);

        WebElement element = (WebElement) wait.until(ExpectedConditions.elementToBeClickable(locator));
        return element;
    }

    protected WebElement waitForElementLocatedByXPath(final String xpath) throws Exception {
        try {
            return waitElementToBeClickable(By.xpath(xpath));
        } catch (WebDriverException e) {
            String msg = "Element not found or not visible for xpath: " + xpath;
            throw new WebDriverException(e);
        }
    }

    protected boolean waitElementToBeDisappear(By locator) throws WebDriverException {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Wait wait = new FluentWait(driver)
                .withTimeout(SELENIUM_ELEMENT_LOCATING_TIME_OUT_SECONDS, SECONDS)
                .pollingEvery(100, MILLISECONDS)
                .ignoring(NoSuchElementException.class)
                .ignoring(StaleElementReferenceException.class);

        return (Boolean) wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public void selectDropDownItem(MobileElement element, String value){
        if(element.getAttribute("text").equals(value)){
            return;
        }
        element.click();
        waitElementToBeClickable(By.xpath("//android.view.View[@content-desc=\"" + value + "\"]"));
        driver.findElement(By.xpath("//android.view.View[@content-desc=\"" + value + "\"]")).click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void scrollToElement(){
    }



}