package listners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.*;

/**
 * @author Banuka Liyanage created on 1/7/2018
 */
public class Listener implements ITestListener, ISuiteListener, IInvokedMethodListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(Listener.class);

    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {

    }

    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {

    }

    public void onStart(ISuite suite) {

    }

    public void onFinish(ISuite suite) {

    }

    public void onTestStart(ITestResult result) {

    }

    public void onTestSuccess(ITestResult result) {
        LOGGER.info("Execution success for the test case :" + result.getTestName() + ".");
    }

    public void onTestFailure(ITestResult result) {
        LOGGER.info("Execution Failed for the test case :" + result.getTestName() + ".");
    }

    public void onTestSkipped(ITestResult result) {
        LOGGER.info("Execution skipped for the test case :" + result.getTestName() + ".");
    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
    }

    public void onStart(ITestContext context) {

    }

    public void onFinish(ITestContext context) {
        LOGGER.info("Execution completed for the test case :" + context.getName() + ".");

    }
}
